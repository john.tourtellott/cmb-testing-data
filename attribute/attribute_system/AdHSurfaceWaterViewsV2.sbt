<?xml version="1.0"?>
<!--Created by XmlV2StringWriter-->
<SMTK_AttributeSystem Version="2">
  <!--**********  Category and Analysis Information ***********-->
  <!--**********  Attribute Definitions ***********-->
  <Definitions />
  <!--**********  Attribute Instances ***********-->
  <Attributes />
  <!--********** Workflow Views ***********-->
  <Views>
    <View Type="Group" Title="SimBuilder" TopLevel="true">
      <DefaultColor>1., 1., 0.5, 1.</DefaultColor>
      <InvalidColor>1, 0.5, 0.5, 1</InvalidColor>
      <Views>
        <View Title="Solvers" />
        <View Title="Time" />
        <View Title="Globals" />
        <View Title="Functions" />
        <View Title="Materials" />
        <View Title="Constituents" />
        <View Title="Friction" />
        <View Title="BoundaryConditions" />
      </Views>
    </View>
    <View Type="Attribute" Title="BoundaryConditions" ModelEntityFilter="e">
      <AttributeTypes>
        <Att Type="VelocityBound" />
        <Att Type="LidElevation" />
        <Att Type="VelocityAndDepthBound" />
        <Att Type="WaterDepthLid" />
        <Att Type="WaterSurfElev" />
        <Att Type="UnitFlow" />
        <Att Type="TotalDischarge" />
        <Att Type="FloatingStationary" />
        <Att Type="DirichletTransport" />
        <Att Type="NaturalTransport" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Constituents">
      <AttributeTypes>
        <Att Type="Constituent" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Friction" CreateEntities="true" ModelEntityFilter="f">
      <AttributeTypes>
        <Att Type="Friction" />
      </AttributeTypes>
    </View>
    <View Type="SimpleExpression" Title="Functions">
      <Att Type="PolyLinearFunction" />
    </View>
    <View Type="Instanced" Title="Globals">
      <InstancedAttributes>
        <Att Name="Globals" Type="Globals" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Materials" CreateEntities="true" ModelEntityFilter="f">
      <AttributeTypes>
        <Att Type="SolidMaterial" />
      </AttributeTypes>
    </View>
    <View Type="Instanced" Title="Solvers">
      <InstancedAttributes>
        <Att Name="Solvers" Type="Solvers" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Time">
      <InstancedAttributes>
        <Att Name="Time" Type="Time" />
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeSystem>
